var addon = require('bindings')('hello');

console.log(addon.hello()); // 'world'
console.log('This should be eight:', addon.add(3, 5))
addon.callback(function(msg){
  console.log(msg); // 'hello world'
});